const env = require('./.env.js')
const { api, rpc } = require('./eosjs')(env.keys, env.endpoint)

const print = (data) => console.log(data.transaction_id)
const ms = require('human-to-milliseconds')
const random = (min, max) => Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min) + 1)) + Math.ceil(min)

const hideErrors = [
  "Contract is already updated.",
  "action queue is empty",
  "duplicate transaction",
  "queued action not ready yet",
  "invalid json response body",
  "Client network socket disconnected",
  "Unexpected token < in JSON",
  "reason: read ETIMEDOUT",
  "Contract is updating, try again later",
  "Contract is updating..."
]

async function tick(tryagain) {

  console.log('tick')
  const actor = env.workerAccount
  const action = {
    account: 'vigorlending',
    name: 'tick',
    authorization: [{ actor, permission: env.workerPermission }],
    data: {},
  }
  let actions = []

  let numActions = 1
  if(!tryagain) numActions = random(5, 20)
  
  for (let step = 0; step < numActions; step++) { actions.push(action) }

  const transact = await api.transact({ actions },
    {
      blocksBehind: 30,
      expireSeconds: random(30, 200)
    }).catch(err => {
      const errString = err.toString()
      const hideError = hideErrors.some(el => errString.match(el))
      if (!hideError) console.error(err.toString())
      else console.log(err.toString())
      if (!tryagain) return
      setTimeout(() => tick(false), ms('10s'))
      return
    })
  if (transact) {
    console.log(transact.transaction_id)
    if (!tryagain) return
    setTimeout(() => tick(false), ms('1s'))
    for (let step = 0; step < 150; step++) {
      setTimeout(() => {
        console.log('doit', step)
        tick(false)
      }, ms(step * 10 + 'ms'))
    }

    return transact
  }
}

tick(false)

setInterval(() => {
  tick(true)
}, ms('5s'))
